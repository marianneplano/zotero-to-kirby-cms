<?php

require __DIR__.'/page-models/zoteroApi.php';

Kirby::plugin('marianne/zotero', [

	'blueprints' => [

		'fields/title' => __DIR__.'/blueprints/fields/title.yml',
		'fields/abstractnote' => __DIR__.'/blueprints/fields/abstractnote.yml',
		'fields/artworkmedium' => __DIR__.'/blueprints/fields/artworkmedium.yml',
		'fields/medium' => __DIR__.'/blueprints/fields/medium.yml',
		'fields/artworksize' => __DIR__.'/blueprints/fields/artworksize.yml',
		'fields/date' => __DIR__.'/blueprints/fields/date.yml',
		'fields/lang' => __DIR__.'/blueprints/fields/lang.yml',
		'fields/shorttitle' => __DIR__.'/blueprints/fields/shorttitle.yml',
		'fields/archive' => __DIR__.'/blueprints/fields/archive.yml',
		'fields/archivelocation' => __DIR__.'/blueprints/fields/archivelocation.yml',
		'fields/librarycatalog' => __DIR__.'/blueprints/fields/librarycatalog.yml',
		'fields/callnumber' => __DIR__.'/blueprints/fields/callnumber.yml',
		'fields/url' => __DIR__.'/blueprints/fields/url.yml',
		'fields/accessdate' => __DIR__.'/blueprints/fields/accessdate.yml',
		'fields/rights' => __DIR__.'/blueprints/fields/rights.yml',
		'fields/extra' => __DIR__.'/blueprints/fields/extra.yml',
		'fields/audiorecordingformat' => __DIR__.'/blueprints/fields/audiorecordingformat.yml',
		'fields/seriestitle' => __DIR__.'/blueprints/fields/seriestitle.yml',
		'fields/volume' => __DIR__.'/blueprints/fields/volume.yml',
		'fields/numberofvolumes' => __DIR__.'/blueprints/fields/numberofvolumes.yml',
		'fields/place' => __DIR__.'/blueprints/fields/place.yml',
		'fields/label' => __DIR__.'/blueprints/fields/label.yml',
		'fields/publisher' => __DIR__.'/blueprints/fields/publisher.yml',
		'fields/runningtime' => __DIR__.'/blueprints/fields/runningtime.yml',
		'fields/isbn' => __DIR__.'/blueprints/fields/isbn.yml',
		'fields/billnumber' => __DIR__.'/blueprints/fields/billnumber.yml',
		'fields/number' => __DIR__.'/blueprints/fields/number.yml',
		'fields/code' => __DIR__.'/blueprints/fields/code.yml',
		'fields/codevolume' => __DIR__.'/blueprints/fields/codevolume.yml',
		'fields/section' => __DIR__.'/blueprints/fields/section.yml',
		'fields/codepages' => __DIR__.'/blueprints/fields/codepages.yml',
		'fields/pages' => __DIR__.'/blueprints/fields/pages.yml',
		'fields/legislativebody' => __DIR__.'/blueprints/fields/legislativebody.yml',
		'fields/session' => __DIR__.'/blueprints/fields/session.yml',
		'fields/history' => __DIR__.'/blueprints/fields/history.yml',
		'fields/blogtitle' => __DIR__.'/blueprints/fields/blogtitle.yml',
		'fields/publicationtitle' => __DIR__.'/blueprints/fields/publicationtitle.yml',
		'fields/websitetype' => __DIR__.'/blueprints/fields/websitetype.yml',
		'fields/type' => __DIR__.'/blueprints/fields/type.yml',
		'fields/series' => __DIR__.'/blueprints/fields/series.yml',
		'fields/seriesnumber' => __DIR__.'/blueprints/fields/seriesnumber.yml',
		'fields/edition' => __DIR__.'/blueprints/fields/edition.yml',
		'fields/numpages' => __DIR__.'/blueprints/fields/numpages.yml',
		'fields/booktitle' => __DIR__.'/blueprints/fields/booktitle.yml',
		'fields/casename' => __DIR__.'/blueprints/fields/casename.yml',
		'fields/court' => __DIR__.'/blueprints/fields/court.yml',
		'fields/datedecided' => __DIR__.'/blueprints/fields/datedecided.yml',
		'fields/docketnumber' => __DIR__.'/blueprints/fields/docketnumber.yml',
		'fields/reporter' => __DIR__.'/blueprints/fields/reporter.yml',
		'fields/reportervolume' => __DIR__.'/blueprints/fields/reportervolume.yml',
		'fields/firstpage' => __DIR__.'/blueprints/fields/firstpage.yml',
		'fields/versionnumber' => __DIR__.'/blueprints/fields/versionnumber.yml',
		'fields/system' => __DIR__.'/blueprints/fields/system.yml',
		'fields/company' => __DIR__.'/blueprints/fields/company.yml',
		'fields/programminglanguage' => __DIR__.'/blueprints/fields/programminglanguage.yml',
		'fields/proceedingstitle' => __DIR__.'/blueprints/fields/proceedingstitle.yml',
		'fields/conferencename' => __DIR__.'/blueprints/fields/conferencename.yml',
		'fields/doi' => __DIR__.'/blueprints/fields/doi.yml',
		'fields/dictionarytitle' => __DIR__.'/blueprints/fields/dictionarytitle.yml',
		'fields/subject' => __DIR__.'/blueprints/fields/subject.yml',
		'fields/encyclopediatitle' => __DIR__.'/blueprints/fields/encyclopediatitle.yml',
		'fields/distributor' => __DIR__.'/blueprints/fields/distributor.yml',
		'fields/genre' => __DIR__.'/blueprints/fields/genre.yml',
		'fields/videorecordingformat' => __DIR__.'/blueprints/fields/videorecordingformat.yml',
		'fields/forumtitle' => __DIR__.'/blueprints/fields/forumtitle.yml',
		'fields/posttype' => __DIR__.'/blueprints/fields/posttype.yml',
		'fields/committee' => __DIR__.'/blueprints/fields/committee.yml',
		'fields/documentnumber' => __DIR__.'/blueprints/fields/documentnumber.yml',
		'fields/interviewmedium' => __DIR__.'/blueprints/fields/interviewmedium.yml',
		'fields/issue' => __DIR__.'/blueprints/fields/issue.yml',
		'fields/seriestext' => __DIR__.'/blueprints/fields/seriestext.yml',
		'fields/journalabbreviation' => __DIR__.'/blueprints/fields/journalabbreviation.yml',
		'fields/issn' => __DIR__.'/blueprints/fields/issn.yml',
		'fields/lettertype' => __DIR__.'/blueprints/fields/lettertype.yml',
		'fields/manuscripttype' => __DIR__.'/blueprints/fields/manuscripttype.yml',
		'fields/maptype' => __DIR__.'/blueprints/fields/maptype.yml',
		'fields/scale' => __DIR__.'/blueprints/fields/scale.yml',
		'fields/country' => __DIR__.'/blueprints/fields/country.yml',
		'fields/assignee' => __DIR__.'/blueprints/fields/assignee.yml',
		'fields/issuingauthority' => __DIR__.'/blueprints/fields/issuingauthority.yml',
		'fields/patentnumber' => __DIR__.'/blueprints/fields/patentnumber.yml',
		'fields/filingdate' => __DIR__.'/blueprints/fields/filingdate.yml',
		'fields/applicationnumber' => __DIR__.'/blueprints/fields/applicationnumber.yml',
		'fields/prioritynumbers' => __DIR__.'/blueprints/fields/prioritynumbers.yml',
		'fields/issuedate' => __DIR__.'/blueprints/fields/issuedate.yml',
		'fields/references' => __DIR__.'/blueprints/fields/references.yml',
		'fields/legalstatus' => __DIR__.'/blueprints/fields/legalstatus.yml',
		'fields/episodenumber' => __DIR__.'/blueprints/fields/episodenumber.yml',
		'fields/audiofiletype' => __DIR__.'/blueprints/fields/audiofiletype.yml',
		'fields/presentationtype' => __DIR__.'/blueprints/fields/presentationtype.yml',
		'fields/meetingname' => __DIR__.'/blueprints/fields/meetingname.yml',
		'fields/programtitle' => __DIR__.'/blueprints/fields/programtitle.yml',
		'fields/network' => __DIR__.'/blueprints/fields/network.yml',
		'fields/reportnumber' => __DIR__.'/blueprints/fields/reportnumber.yml',
		'fields/reporttype' => __DIR__.'/blueprints/fields/reporttype.yml',
		'fields/institution' => __DIR__.'/blueprints/fields/institution.yml',
		'fields/nameofact' => __DIR__.'/blueprints/fields/nameofact.yml',
		'fields/codenumber' => __DIR__.'/blueprints/fields/codenumber.yml',
		'fields/publiclawnumber' => __DIR__.'/blueprints/fields/publiclawnumber.yml',
		'fields/dateenacted' => __DIR__.'/blueprints/fields/dateenacted.yml',
		'fields/thesistype' => __DIR__.'/blueprints/fields/thesistype.yml',
		'fields/university' => __DIR__.'/blueprints/fields/university.yml',
		'fields/studio' => __DIR__.'/blueprints/fields/studio.yml',
		'fields/websitetitle' => __DIR__.'/blueprints/fields/websitetitle.yml',
		'fields/version' => __DIR__.'/blueprints/fields/version.yml',
		'fields/collections' => __DIR__.'/blueprints/fields/collections.yml',
		'fields/dateadded' => __DIR__.'/blueprints/fields/dateadded.yml',
		'fields/datemodified' => __DIR__.'/blueprints/fields/datemodified.yml',
		'fields/general' => __DIR__.'/blueprints/fields/general.yml',
		'fields/creators' => __DIR__.'/blueprints/fields/creators.yml',
		'fields/order' => __DIR__.'/blueprints/fields/order.yml',
		'fields/relations' => __DIR__.'/blueprints/fields/relations.yml',
		'fields/temprelations' => __DIR__.'/blueprints/fields/temprelations.yml',
		'tabs/meta' => __DIR__.'/blueprints/tabs/meta.yml',
		'pages/artwork' => __DIR__.'/blueprints/pages/artwork.yml',
		'pages/attachment' => __DIR__.'/blueprints/pages/attachment.yml',
		'pages/audiorecording' => __DIR__.'/blueprints/pages/audiorecording.yml',
		'pages/bill' => __DIR__.'/blueprints/pages/bill.yml',
		'pages/blogpost' => __DIR__.'/blueprints/pages/blogpost.yml',
		'pages/book' => __DIR__.'/blueprints/pages/book.yml',
		'pages/booksection' => __DIR__.'/blueprints/pages/booksection.yml',
		'pages/case' => __DIR__.'/blueprints/pages/case.yml',
		'pages/computerprogram' => __DIR__.'/blueprints/pages/computerprogram.yml',
		'pages/conferencepaper' => __DIR__.'/blueprints/pages/conferencepaper.yml',
		'pages/dictionaryentry' => __DIR__.'/blueprints/pages/dictionaryentry.yml',
		'pages/document' => __DIR__.'/blueprints/pages/document.yml',
		'pages/email' => __DIR__.'/blueprints/pages/email.yml',
		'pages/encyclopediaarticle' => __DIR__.'/blueprints/pages/encyclopediaarticle.yml',
		'pages/film' => __DIR__.'/blueprints/pages/film.yml',
		'pages/forumpost' => __DIR__.'/blueprints/pages/forumpost.yml',
		'pages/hearing' => __DIR__.'/blueprints/pages/hearing.yml',
		'pages/instantmessage' => __DIR__.'/blueprints/pages/instantmessage.yml',
		'pages/interview' => __DIR__.'/blueprints/pages/interview.yml',
		'pages/journalarticle' => __DIR__.'/blueprints/pages/journalarticle.yml',
		'pages/letter' => __DIR__.'/blueprints/pages/letter.yml',
		'pages/magazinearticle' => __DIR__.'/blueprints/pages/magazinearticle.yml',
		'pages/manuscript' => __DIR__.'/blueprints/pages/manuscript.yml',
		'pages/map' => __DIR__.'/blueprints/pages/map.yml',
		'pages/newspaperarticle' => __DIR__.'/blueprints/pages/newspaperarticle.yml',
		'pages/note' => __DIR__.'/blueprints/pages/note.yml',
		'pages/patent' => __DIR__.'/blueprints/pages/patent.yml',
		'pages/podcast' => __DIR__.'/blueprints/pages/podcast.yml',
		'pages/presentation' => __DIR__.'/blueprints/pages/presentation.yml',
		'pages/radiobroadcast' => __DIR__.'/blueprints/pages/radiobroadcast.yml',
		'pages/report' => __DIR__.'/blueprints/pages/report.yml',
		'pages/statute' => __DIR__.'/blueprints/pages/statute.yml',
		'pages/thesis' => __DIR__.'/blueprints/pages/thesis.yml',
		'pages/tvbroadcast' => __DIR__.'/blueprints/pages/tvbroadcast.yml',
		'pages/videorecording' => __DIR__.'/blueprints/pages/videorecording.yml',
		'pages/webpage' => __DIR__.'/blueprints/pages/webpage.yml',
		'pages/library' => __DIR__.'/blueprints/pages/library.yml',
		'pages/collection' => __DIR__.'/blueprints/pages/collection.yml',
		'pages/transition' => __DIR__.'/blueprints/pages/transition.yml',
		'site' => __DIR__.'/blueprints/site.yml'

	],

	'templates' => [

		'update' => __DIR__.'/templates/update.php'
	],

	'pageModels' => [

		'update' => 'ZoteroPage',
		'artwork' => 'ZoteroPage',
		'attachment' => 'ZoteroPage',
		'audiorecording' => 'ZoteroPage',
		'bill' => 'ZoteroPage',
		'blogpost' => 'ZoteroPage',
		'book' => 'ZoteroPage',
		'booksection' => 'ZoteroPage',
		'case' => 'ZoteroPage',
		'collection' => 'ZoteroPage',
		'computerprogram' => 'ZoteroPage',
		'conferencepaper' => 'ZoteroPage',
		'dictionaryentry' => 'ZoteroPage',
		'document' => 'ZoteroPage',
		'email' => 'ZoteroPage',
		'encyclopediaarticle' => 'ZoteroPage',
		'film' => 'ZoteroPage',
		'forumpost' => 'ZoteroPage',
		'hearing' => 'ZoteroPage',
		'instantmessage' => 'ZoteroPage',
		'interview' => 'ZoteroPage',
		'journalarticle' => 'ZoteroPage',
		'letter' => 'ZoteroPage',
		'magazinearticle' => 'ZoteroPage',
		'manuscript' => 'ZoteroPage',
		'map' => 'ZoteroPage',
		'newspaperarticle' => 'ZoteroPage',
		'note' => 'ZoteroPage',
		'patent' => 'ZoteroPage',
		'podcast' => 'ZoteroPage',
		'presentation' => 'ZoteroPage',
		'radiobroadcast' => 'ZoteroPage',
		'report' => 'ZoteroPage',
		'statute' => 'ZoteroPage',
		'thesis' => 'ZoteroPage',
		'tvbroadcast' => 'ZoteroPage',
		'videorecording' => 'ZoteroPage',
		'webpage' => 'ZoteroPage'
	],

	'pages' => [

		'update' => new ZoteroPage([

			'slug'     => 'update',
			'template' => 'update'

		])
	],

	'hooks' => [

		'page.update:before' => function ($page, $values, $strings){

			$page->changeRemote($values, $strings);
			$myfile = fopen("debug2.html", 'a');
			fwrite($myfile, $page->title()->toString().'<br>');
		},

		'page.delete:before' => function ($page, $force){

			$page->deleteRemote();
		},

		'page.changeTemplate:before' => function ($page, $template){

			$page->changeRemoteTemplate($page, $template);

		},


		'page.changeTitle:before' => function ($page, $title, $languageCode){

			$page->changeRemoteTitle($page, $title);

		},

		'page.create:after' => function ($page){

			$page->createRemote();
		},

		'page.update:after' => function($newPage, $oldPage){
			$newPage->newVersion();
		 $myfile = fopen("debug2.html", 'a');
			fwrite($myfile, $newPage->title()->toString()." after<br>");},
		'page.changeTemplate:after' => function($newPage, $oldPage){ $newPage->newVersion(); },
		'page.changeTitle:after' => function($newPage, $oldPage){ $newPage->newVersion(); }
	]
]);
