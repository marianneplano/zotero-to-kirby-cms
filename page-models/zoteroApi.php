<?php

class ZoteroPage extends Page{

	private static $_lastModif;
	private static $_writeVersion = 0;
	private static $_callApi = true;
	private static $_createPages = true;
	private static $_modifCount = 0;
	private static $_createCount = 0;
	private static $_delCount = 0;
	private static $_relatedIds = [];
	private static $_delPages = true;

	/* GENERAL */

	public function now(){

		$d = new Datetime();

		return $d->format('Y-m-d H:i');
	}


	/* --- CURL --- */

	/* CURL: PARSING */

	public function parseName($str){

		$first = strpos($str, '"')+1;
		$last = strrpos($str, '"');

		return substr($str, $first, $last-$first);
	}

	public function parseEndpoint($str){

		$str=trim($str);

		return substr($str, strrpos($str, "/"), strlen($str)-strrpos($str, "/")-1);
	}

	public function parseLink($link){

		if(strpos($link, ";")){

			$parsed = [];
			$arr = explode(";", $link);

			foreach($arr as $k=>$item){

				if($k == 0){

					$parsed["first"] = $this->parseEndpoint($item);

				}else{

					if(strpos($item, ",")){

						$cut = explode(",", $item);
						$name = $this->parseName($cut[0]);

						$parsed[$name] = $this->parseEndpoint($cut[1]);

					}else{

						$parsed[$this->parseName($item)] = null;
					}

				}

			}

			return $parsed;

		}else{

			return null;
		}

	}

	public function parseSeconds($retry){

		return intval(substr($retry, strpos($retry, "<")+1, strpos($retry, ">")-strpos($retry, "<")-1));

	}

	public function get_headers_from_curl_response($response){

		$headers = array();

		$header_text = substr($response, 0, strpos($response, "\r\n\r\n"));

		foreach (explode("\r\n", $header_text) as $i => $line){

			if ($i === 0){
				$headers['http_code'] = $line;
			}else{
				list ($key, $value) = explode(': ', $line);

				$headers[$key] = $value;
			}

		}

		return $headers;
	}


	/* CURL: QUERY */

	public function curl($url, $action, $version=0, $data=false){

		$ch = curl_init();
		$apiKey = $this->kirby()->option('ZoteroAPIKey');
		$headers = [];

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $action);

		if(!strpos($url, "delete")){

			if($action == "GET"){

				if($this->libraryPage()->version()->isNotEmpty()){

					$headers[] = "If-Modified-Since-Version: ".$this->libraryPage()->version();
				}

			}elseif($action == "PATCH" || $action == "DELETE"){

				$headers[] = "If-Unmodified-Since-Version: ".$version;

			}
		}

		if($action == "POST"){

			$headers [] = "Content-Type: application/json";

		}else{

			$headers[] = "Accept: application/json";

		}

		$headers[] = "Authorization: Bearer ".$apiKey;

		if($action !== "GET"){

			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		}

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if (curl_errno($ch)) {

			echo 'Error:' . curl_error($ch);
		}

		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$headers = substr($result, 0, $header_size);
		$body = substr($result, $header_size);

		curl_close ($ch);

		return ["code" => $httpcode, "headers" => $headers, "body" => $body];
	}



	/* --- ADIMIN KIRBY --- */

	protected function logZoteroUser(){

		$user = $this->kirby()->user($this->kirby()->option("ZoteroUserMail"));
		$user->login($this->kirby()->option("ZoteroUserPassword"));

	}

	/* --- API ACCESS --- */

	public function zoteroBaseUrl(){

		$userID = $this->kirby()->option('ZoteroAPIUserID');

		return "https://api.zotero.org/users/".$userID;
	}

	public function addParams($endpoint, $params){

		$string = [];

		foreach($params as $name => $value){

			if($name == "since"){

				if($value == true && $this->libraryPage()->version()->isNotEmpty()){

					$string [] = "since=".$this->libraryPage()->version()->toString();
				}

			}else{

				$string [] = $name."=".$value;

			}

		}

		$string = "?".implode("&", $string);
		$string = $string;

		return $endpoint.$string;
	}


	public function queryApi($endpoint, $action="GET", $version=0, $data=false){

		$results = $this->curl($this->zoteroBaseUrl().$endpoint, $action, $version, $data);
		$headers = $this->get_headers_from_curl_response($results["headers"]);
		$tryAfter = [429, 503];

		if(!in_array($results["code"], $tryAfter)){

			return $results;

		}elseif(isset($headers["Retry-After"])){

			$sec = $this->parseSeconds($headers["Retry-After"]);
			sleep($sec);
			$this->queryApi($endpoint, $action, $version, $data);
		}
	}

	/* --- USE API --- */

	/* USE: ZOTERO DATA */

	public function zotTemplates(){

		return json_decode(file_get_contents("site/plugins/zotero/zotero-datas/templates.json"), "true");

	}

	public function zotTemplatesLower(){

		$temp = $this->zotTemplates();

		foreach($temp as $k=>$t){

			$temp[$k] = strtolower($t);

		}

		return $temp;
	}

	public function isItem(){

		return (in_array($this->template(), $this->zotTemplatesLower()) || in_array($this->intendedTemplate(), $this->zotTemplatesLower()));
	}

	public function isCollection(){

		return ($this->template() == "collection" || $this->intendedTemplate() == "collection");
	}

	public function isZotPage(){

		return ($this->isItem() || $this->isCollection());
	}

	public function reCaseField($field){

		$caseds = json_decode(file_get_contents("site/plugins/zotero/zotero-datas/fields.json"), "true");

		foreach($caseds as $cased){

			if(strtolower($cased) == $field){

				return $cased;
			}
		}

		return $field;
	}

	/* USE: TRANSLATING DATA */

	protected function isSpecial($field){

		return in_array($field, ["creators", "collections", "tags", "relations", "parentCollection"]);
	}


	private function parsePageID($url){

		$start = strrpos($url, "/")+1;

		return strtolower(substr($url, $start));
	}


	/* --- READ API --- */

	/* READ: UPDATE - SPECIAL FIELDS */

	protected function invalidFields(){

		return ["attachment", "note"];

	}

	private function creatorsToKirby($data){

		$creators = [];

		foreach($data as $k=>$creator){

			$creators [$k] = [];
			$creators [$k]["creatorType"] = $creator->creatorType;

			if(property_exists($creator, "name")){

				$creators [$k]["name"] = $creator->name;

			}

			if(property_exists($creator, "firstName")){

				$creators [$k]["firstname"] = $creator->firstName;

			}

			if(property_exists($creator, "lastName")){

				$creators [$k]["lastname"] = $creator->lastName;

			}

		}

		return $creators;

	}

	private function collectionsToKirby($data){

		if($data !== false){

			$pages = [];
			$data = (is_array($data)) ? $data : [$data];

			foreach($data as $collection){

				$pages [] = "library/".strtolower($collection);

			}

			return $pages;

		}else{

			return "";
		}
	}

	private function tagsToKirby($data){

		$tags = [];

		foreach($data as $tag){

			$tags [] = $tag->tag;

		}

		return implode(",", $tags);
	}

	private function relationToKibyURL($url){

		return "library/".$this->parsePageID($url);
	}

	private function relationsToKirby($data, $id){

		$data = (array) $data;

		if(isset($data["dc:relation"])){

			$relations = (is_array($data["dc:relation"])) ? $this->relationToKibyURL($data["dc:relation"]) : [$this->relationToKibyURL($data["dc:relation"])];

			self::$_relatedIds [] = $id;

			return implode(",", $relations);

		}else{

			return "";
		}
	}


	private function getReadSpecialField($field, $data, $id){

		if($field == "creators"){

			return $this->creatorsToKirby($data);

		}elseif($field == "collections"){

			return $this->collectionsToKirby($data);

		}elseif($field == "tags"){

			return $this->tagsToKirby($data);

		}else{

			return $this->relationsToKirby($data, $id);
		}

	}

	private function hasRelations($item){

		return $item->data->relations !== "";
	}


	/* READ: UPDATE - ITEM PAGES */

	private function makeItemContent($library, $item){

		$datas = (array) $item->data;
		$outs = ["key", "itemType"];
		$content = [];

		foreach($datas as $name => $data){

			if(!in_array($name, $outs)){

				$k = ($name !== "language") ? strtolower($name) : "lang";
				$pageExists = $this->subpageExists($library, $item);

				if($name == "relations"){

					$k = "temprelations";
				}

				$content[$k] = ($this->isSpecial($name)) ? $this->getReadSpecialField($name, $data, $datas["key"]) : $data;

			}

		}

		$content["version"] = intval($item->version);

		return $content;

	}

	private function makeItems($items){

		if(count($items) > 0){

			$library = $this->libraryPage();

			foreach($items as $item){

				$dateModif = strtotime($item->data->dateModified);
				$lastUpdate = ($this->library()->lastUpdate()->isNotEmpty()) ? intval($this->library()->lastUpdate()->toString()) : 0;

				if(!in_array($item->data->itemType, $this->invalidFields())){

					$pageContent = $this->makeItemContent($library, $item);
					$newPage = $this->makePage($item, $pageContent, $item->data->itemType);

					if($newPage == false){

						break;
					}
				}

			}

		}
	}

	public function addRelations(){

		$ids = self::$_relatedIds;

		if(count($ids) > 0){

			foreach ($ids as $id){

				$item = $this->libraryPage()->findPageOrDraft($id);
				$item->update(["relations" => explode(",", $item->temprelations()->toString()), "temprelations" => ""]);
			}

		}
	}

	/* READ: UPDATE - COLLECTION PAGES */

	private function makeCollectionPage($collection){

		$slug = $collection->key;
		$pageContent = ['title'  => $collection->data->name, 'version' => intval($collection->version), 'parentCollection' => $this->collectionsToKirby($collection->data->parentCollection)];
		$newPage = $this->makePage($collection, $pageContent, "collection");

		return $newPage;

	}

	private function makeCollections($collections){

		if(count($collections) > 0){

			foreach($collections as $collection){

				$newPage = $this->makeCollectionPage($collection);

			}

		}
	}

	/* READ: UPDATE - CREATE PAGES */

	protected function getPageType($endpoint){

		$type = (strpos($endpoint, "?")) ? substr($endpoint, 1, strrpos($endpoint, "?")-1) : substr($endpoint, 1);

		return $type;

	}

	private function needsChange($oldPage, $incoming){

		return intval($oldPage->version()->toString()) !== $incoming->version;
	}

	private function subpageExists($parent, $item){

		return $parent->findPageOrDraft(strtolower($item->key)) !== null;

	}

	private function makePage($item, $pageContent, $template){

		$lib = $this->libraryPage();
		$pageExists = $this->subpageExists($lib, $item);

		if(!$pageExists){

			self::$_createCount ++;

			$publish = ($this->kirby()->option('ZoteroAutoPublish') !== null) ? $this->kirby()->option('ZoteroAutoPublish') : false;

			$newPage = $lib->createChild(['slug' => $item->key, 'template' => $template, 'content' => $pageContent], null, true);

			if($publish == true){

				try{

					$newPage->changeStatus("listed");

				}catch (Exception $e){

					echo "Error on page ".$newPage->title().": ".$e->getMessage().". Please check this page in the panel for more informations (url: ".$newPage->uri().")<br>";
				}
			}

		}else{

			$oldPage = $lib->findPageOrDraft($item->key);

			if($this->needsChange($oldPage, $item)){

				self::$_modifCount ++;
				$newPage = $oldPage->update($pageContent);

			}else{

				return false;
			}
		}

		return $newPage;
	}


	protected function makePages($response, $endpoint){

		$data = json_decode($response);

		if(count($data) > 0){

			$pageType = $this->getPageType($endpoint);

			if($pageType == "collections"){

				$this->makeCollections($data);

			}elseif($pageType == "items"){

				$this->makeItems($data);

			}

		}

	}


	/* READ: UPDATE - PAGINATING */

	protected function paginate($endpoint, $limit=100){

		$endpoint = $this->addParams($endpoint, ["limit" => $limit, "since" => true]);
		$results = $this->queryApi($endpoint);

		$httpcode = $results["code"];
		$headers = $this->get_headers_from_curl_response($results["headers"]);


		if($httpcode == 200){

			$links = (isset($headers['Link'])) ? $this->parseLink($headers['Link']) : null;

			$this->makePages($results["body"], $endpoint);

			if($links !== null && isset($links["next"])){

				$this->paginate($links["next"]);

			}else{

				self::$_writeVersion = intval($headers["Last-Modified-Version"]);
			}

		}elseif($httpcode !== 304){

			throw new Exception("Zotero API: ".$results["code"]." — ".$results["body"]);
		}

		return ($httpcode == 304) ? false : true;

	}


	/* READ: UPDATE - LAUNCHING */

	public function updatePages(){

		$this->logZoteroUser();
		$this->makeLibraryPage();

		$updateCollecs = $this->paginate("/collections");

		if($updateCollecs == true){

			$updateItems = $this->paginate("/items");
			$this->addRelations();
			$this->deleteLocalItems();

			$this->libraryPage()->update(["version" => self::$_writeVersion]);

			echo "Pages created: ".self::$_createCount."<br>";
			echo "Pages modified: ".self::$_modifCount."<br>";
			echo "Pages deleted: ".self::$_delCount."<br>";



		}else{

			echo "Library already up to date!";
		}

	}

	public function loadZoteroResources(){

		self::$_callApi = false;
		self::$_createPages = false;

		$this->kirby()->site()->purge();
		$this->updatePages();
		$this->updateLib(['lastupdate' => $this->now(), 'version' => self::$_lastModif]);

	}

	/* READ: LIBRARY PAGE */

	public function libraryPage(){

		return $this->kirby()->site()->find("library");
	}

	private function updateLib($array){

		$lib = $this->libraryPage();
		$lib->update($array);

	}

	protected function makeLibraryPage(){

		$site = $this->kirby()->site();
		$hasLibrary = $site->find('library');

		if($hasLibrary == null){

			$lib = $site->createChild(['slug' => 'library', 'template' => 'library', 'content' => ['title'  => 'Library'], 'draft' => 0]);
			$lib->changeStatus("listed");
		}

	}

	/* READ: DELETE ITEMS + COLLECTIONS */

	private function deleteLocalItems(){

		$results = $this->queryApi($this->addParams("/deleted", ["since" => true]));
		self::$_delPages = false;
		$categories = json_decode($results["body"], true);

		if(is_array($categories)){

			foreach($categories as $itemType => $ids){

				if(!in_array($itemType, ["searches", "tags", "settings"])){

					if(count($ids) > 0){

						foreach($ids as $id){

							$lib = $this->libraryPage();
							$page = $lib->findPageOrDraft(strtolower($id));

							if($page !== null){

								$page->delete();

								self::$_delCount = self::$_delCount + 1;

							}
						}

					}
				}
			}
		}

	}


	/* --- WRITE TO API --- */

	/* WRITE: ERROR HANDLING */

	protected function throwWriteError($results){

		if($results["code"] !== 204){

			$plus = ($results["code"] == 412) ? " Try to update the content first. (/update)" : "";

			throw new Exception("Zotero API => Error ".$results["code"]." ".$results["body"].$plus);
		}
	}

	/* WRITE: UPDATE — FIELDS */

	public function ignoreFields(){

		return ["dateadded", "datemodified", "spec", "gen"];
	}

	private function getItemID($uri){

		return strtoupper(substr($uri, strlen("library/")));

	}

	private function collectionsToZotero($urls, $field){

		$collecs = [];

		foreach(Data::decode($urls, 'yaml') as $url){

			$collecs [] = $this->getItemID($url);
		}

		return ($field == "collections") ? $collecs : $collecs[0];
	}

	private function creatorsToZotero($creators){

		$creators = Data::decode($creators, 'yaml');
		$new = [];

		foreach ($creators as $k=>$creator){

			$new[$k]["creatorType"] = $creator["creatortype"];

			if($creator["firstname"] == "" && $creator["lastname"] == ""){

				$new[$k]["name"] = $creator["name"];

			}else{

				$new[$k]["firstName"] = $creator["firstname"];
				$new[$k]["lastName"] = $creator["lastname"];

			}

		}

		file_put_contents("creators.json", json_encode($new));

		return $new;


	}

	public function addNewRelations($news, $olds, $baseUrl){

		$replaces = [];

		foreach($news as $new){

			$replaces [] = $baseUrl."/".strtoupper($this->parsePageID($new));

			if(!in_array($new, $olds)){

				$matchPage = $this->libraryPage()->find($new);

				if($matchPage->relations()->isNotEmpty()){

					if($matchPage->relations()->toPages()->find($this->id()) == false){

						$relations = [];

						foreach($matchPage->relations()->toPages() as $matchRelation){

							$relations [] = $matchRelation->id();
						}

						$relations [] = $this->id();

						$matchPage->update(["relations" => $relations]);

					}

				}else{

					$matchPage->update(["relations" => [$this->id()]]);
				}

			}
		}

		return (count($replaces) == 1) ? $replaces[0] : $replaces;
	}


	public function removeDeletedRelations($news, $olds, $baseUrl){

		$replaces = [];

		foreach($olds as $old){

			if(!in_array($old, $news)){

				$replaces [] = $baseUrl."/".strtoupper($this->parsePageID($old));
				$matchPage = $this->libraryPage()->find($old);
				$relations = [];

				foreach($matchPage->relations()->toPages() as $matchRelation){

					if($matchRelation->id() !== $this->id()){

						$relations [] = $matchRelation->id();

					}
				}

				$matchPage->update(["relations" => $relations]);
			}
		}

		return (count($replaces) == 1) ? $replaces[0] : $replaces;
	}


	private function relationsToZotero($relations, $oldrelations){

		$baseUrl = "http://zotero.org/users/7030281/items";
		$relations = Data::decode($relations, "yaml");
		$oldrelations = Data::decode($oldrelations, "yaml");

		$back = [];

		$back ["dc:relation"] = $this->addNewRelations($relations, $oldrelations, $baseUrl);
		$back ["dc:replaces"] = $this->removeDeletedRelations($relations, $oldrelations, $baseUrl);

		return $back;

	}

	private function tagsToZotero($tags){

		$array = [];

		foreach(Data::decode($tags, 'yaml') as $item){

			$tag["tag"] = $item;
			$array [] = $tag;
		}

		return $array;
	}

	private function getWriteSpecialField($field, $newvalue, $oldvalue){

		if($field == "collections" || $field == "parentcollection"){

			return $this->collectionsToZotero($newvalue, $field);

		}elseif($field == "creators"){

			return $this->creatorsToZotero($newvalue);

		}elseif($field == "relations"){

			return $this->relationsToZotero($newvalue, $oldvalue);

		}elseif($field == "accessdate"){

			return '';

		}else{

			return $this->tagsToZotero($newvalue);

		}

	}

	/* WRITE: UPDATE ITEM */

	public function updatedValues($newstrings){

		$oldValues = $this->content()->data();
		$updatedData = [];


		ob_start();

		foreach($oldValues as $field => $oldvalue){

			if(!in_array($field, $this->ignoreFields())){

				$newvalue = trim($newstrings[$field]);

				if($oldvalue !== $newvalue){

					$key = $this->reCaseField($field);

					if($this->isSpecial($key)){

						$updatedData [$key] = $this->getWriteSpecialField($field, $newvalue, $oldvalue);

					}elseif($field == "lang"){

						$updatedData ["language"] = $newvalue;

					}else{

						$updatedData [$key] = $newvalue;
					}
				}

			}
		}

		return $updatedData;

	}

	/* WRITE: UPDATE ITEM + COLLECTION */

	private function patchToZotero($newValues, $type="item"){

		$endpoint = $this->addParams("/".$type."s/".strtoupper($this->slug()), ["slug" => true]);
		$results = $this->queryApi($endpoint, "PATCH", $this->version()->toString(), json_encode($newValues));
		$headers = $this->get_headers_from_curl_response($results["headers"]);

		$this->throwWriteError($results);

		$version = intval($headers["Last-Modified-Version"]);
		self::$_writeVersion = $version;

		file_put_contents("debug3", $version);

	}


	public function changeRemote($values, $strings){

		self::$_createPages = false;

		if(self::$_callApi == true && $this->isZotPage()){

			self::$_callApi = false;

			$newValues = $this->updatedValues($strings);

			if(!empty($newValues)){

				$type = ($this->isCollection()) ? "collection" : "item";

				$this->patchToZotero($newValues, $type);
			}

		}

	}

	/* WRITE: DELETE ITEM + COLLECTION */

	public function deleteFromRelations(){

		$relations = $this->relations();

		if($relations->isNotEmpty()){

			foreach($relations->toPages() as $relatedPage){

				$new = [];

				foreach($relatedPage->relations()->toPages() as $subRelated){

					if($subRelated->id() !== $this->id()){

						$new [] = $subRelated->id();
					}

				}

				$relatedPage->update(["relations" => $new]);
			}
		}
	}

	public function deleteRemote(){

		self::$_callApi = false;

		if($this->isZotPage() && self::$_delPages == true){

			$type = ($this->isCollection()) ? "collection" : "item";
			$endpoint = "/".$type."s/".strtoupper($this->slug());
			$results = $this->queryApi($endpoint, "DELETE", $this->version());
			$this->throwWriteError($results);

		}
	}

	/* WRITE: NEW ITEM + COLLECTION */

	public function newItem($template){

		$datas = [];

		$datas["itemType"] = $template;
		$datas["title"] = $this->title()->toString();
		$datas["tags"] = [];
		$datas["collections"] = [];
		$datas["relations"] = new stdClass;

		return $datas;

	}

	public function newCollection(){

		$datas = [];

		$datas["name"] = $this->title()->toString();
		$datas["parentCollection"] = "";

		return $datas;
	}

	public function createRemote(){

		self::$_callApi = false;

		if($this->isZotPage() && self::$_createPages == true){

			$this->logZoteroUser();

			$template = $this->intendedTemplate()->name();

			if($this->isItem()){

				$datas = $this->newItem();
				$endpoint = "/items";

			}else{

				$datas = $this->newCollection();
				$endpoint = "/collections";
			}

			$results = $this->queryApi($endpoint, "POST", 0, json_encode([$datas]));

			$body = $results["body"];
			$slug = strtolower(json_decode($body, true)["successful"][0]["key"]);

			if($this->isItem()){

				$newTemp = $this->changeTemplate("transition");
				$newSlug = $newTemp->changeSlug($slug);
				$newSlug->changeTemplate($template);

			}else{

				$this->changeSlug($slug);
			}

		}
	}

	/* WRITE: CHANGE ITEM TEMPLATE */

	public function changeRemoteTemplate($page, $template){

		if($this->isItem() && self::$_callApi == true){

			$newValues = ["itemType" => $template];

			$this->patchToZotero($newvalues);

		}
	}

	/* WRITE: CHANGE ITEM + COLLEC */

	public function changeRemoteTitle($page, $title){

		if($this->isZotPage() && self::$_callApi == true){

			$newValues = ($this->isCollection()) ? ["name" => $title] : ["title" => $title];
			$type = ($this->isCollection()) ? "collection" : "item";

			$this->patchToZotero($newValues, $type);

		}
	}

	public function newVersion(){

		if(self::$_writeVersion !== 0){

			$this->update(["version" => self::$_writeVersion]);
		}

	}

}
